<?php
/**
 * TaskProduser.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @category Common
 * @package SearchEngine
 */

namespace iWeekender\SearchEngine;

use iWeekender\SearchEngine\Exceptions\ExceptionMessagesInterface as EM;
use iWeekender\Request\Search\SearchRequest;
use iWeekender\Contract\Configs\SearchEngineConfigInterface;
use iWeekender\Contract\Request\Search\ApiSearchRequestInterface;
use iWeekender\Exceptions\IWExceptionConfiguration;
use iWeekender\Exceptions\IWExceptionValidation;
use iWeekender\Exceptions\IWException;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Psr\Log\LoggerInterface;
use Exception;

/**
 * [queue: "task.flight.request"] Split a large flight search request into search task part from a specific supplier and put task in the queue.
 */
final class TaskProduser
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SearchEngineConfigInterface
     */
    private $searchEngineConfig;

    /**
     * @var ApiSearchRequestInterface
     */
    private $apiSearchRequest;

    /**
     * @var array
     */
    private $supplierCollection;

    /**
     * @var AmqpEngine
     */
    private $amqpEngine;

    private $connection = null;
    private $channel = null;

    /**
     * TaskProduser constructor.
     * @param LoggerInterface $logger
     * @param SearchEngineConfigInterface $searchEngineConfig
     * @param ApiSearchRequestInterface $apiSearchRequest
     * @param array $supplierCollection
     * @throws IWException
     * @throws IWExceptionConfiguration
     */
    public function __construct(
        LoggerInterface $logger,
        SearchEngineConfigInterface $searchEngineConfig,
        ApiSearchRequestInterface $apiSearchRequest,
        array $supplierCollection
    ) {
        $this->logger = $logger;
        $this->searchEngineConfig = $searchEngineConfig;
        $this->apiSearchRequest = $apiSearchRequest;
        $this->supplierCollection = $supplierCollection;

        $this->connection = new AMQPStreamConnection(
            $searchEngineConfig->getAmqpHost(),
            $searchEngineConfig->getAmqpPort(),
            $searchEngineConfig->getAmqpUser(),
            $searchEngineConfig->getAmqpPassword()
        );

        if ($this->connection->isConnected()) {
            $this->channel = $this->connection->channel();

            $this->amqpEngine = new AmqpEngine($searchEngineConfig, $this->channel);

            $this->createQueueLog();
            $this->createQueueTaskRequest();
            $this->createExchanges();
            $this->createBinds();
        } else {
            throw new IWException(EM::MES_AMPQ_IS_NOT_CONNECT);
        }
    }

    /**
     * TaskProduser destructor.
     * @throws Exception
     */
    public function __destruct() {
        if (!empty($this->channel)) {
            $this->channel->close();
        }
        if (!empty($this->connection)) {
            $this->connection->close();
        }
    }

    /**
     * Main method. Split a large flight search request into search task part from a specific supplier and put task in the queue.
     * @throws IWException
     * @throws IWExceptionConfiguration
     * @throws IWExceptionValidation
     */
    public function produse(): void {
        $this->splitSearchRequestBaseOnSupplierCollection();
        $this->createQueueTaskResponse();
    }

    /**
     * Create a queue for Logging.
     */
    private function createQueueLog(): void {
        $this->amqpEngine->createQueueLog();
    }

    /**
     * Create a queue for Request.
     * @throws IWExceptionConfiguration
     */
    private function createQueueTaskRequest(): void {
        $this->amqpEngine->createQueueTaskRequest($this->apiSearchRequest->getServiceType());
    }

    /**
     * Create a queue for Response.
     * @throws IWExceptionConfiguration
     */
    private function createQueueTaskResponse() {
        /*durable = false nowait = true*/
        $this->channel->queue_declare(
            $this->amqpEngine->getQueueTaskResponse($this->apiSearchRequest->getServiceType(), $this->apiSearchRequest->getRequestID()),
            false,
            false,
            false,
            false,
            false,
            $this->amqpEngine->getTaskResponseArg($this->apiSearchRequest->getServiceType())
        );
    }

    /**
     * Create AMQP Exchanges
     */
    private function createExchanges() {
        $this->channel->exchange_declare($this->amqpEngine->getExchangeTask($this->apiSearchRequest->getServiceType()), 'topic', false, true, false);
    }

    /**
     * Bing AMQP Exchanges with Request and Logging queues.
     */
    private function createBinds() {
        $exchange = $this->amqpEngine->getExchangeTask($this->apiSearchRequest->getServiceType());
        $this->channel->queue_bind($this->amqpEngine->getQueueTaskRequest($this->apiSearchRequest->getServiceType()), $exchange);
        $this->channel->queue_bind($this->amqpEngine->getQueueLog(), $exchange);
    }

    /**
     * @param string $supplier
     * @return string
     * @throws IWException
     * @throws IWExceptionValidation
     */
    private function createSearchRequest(string $supplier): string {
        $data = $this->apiSearchRequest->getAssociativeArray();
        SearchRequest::setSupplierToArray($data, $supplier);
        $searchRequest = new SearchRequest();
        $searchRequest->loadToObject($data);
        return $searchRequest->getRequest(null);
    }

    /**
     * @throws IWException
     * @throws IWExceptionValidation
     */
    private function splitSearchRequestBaseOnSupplierCollection() {
        foreach ($this->supplierCollection as $supplier) {
            $request = $this->createSearchRequest($supplier);
            $ampqMessage = $this->amqpEngine->createAMQPMessage($request, $this->apiSearchRequest->getServiceType(), $this->apiSearchRequest->getRequestID());
            $this->channel->basic_publish($ampqMessage, $this->amqpEngine->getExchangeTask($this->apiSearchRequest->getServiceType()));
        }
    }
}
