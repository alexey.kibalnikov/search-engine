<?php
/**
 * TaskConsumer.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @category Common
 * @package SearchEngine
 */

namespace iWeekender\SearchEngine;

use iWeekender\SearchEngine\Exceptions\ExceptionMessagesInterface as EM;
use IWeekender\Contract\Request\Search\ApiSearchRequestInterface;
use iWeekender\Contract\Configs\SearchEngineConfigInterface;
use iWeekender\Exceptions\IWException;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Exception;
use ErrorException;

/**
 * Get all supplier responses.
 */
final class TaskConsumer
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SearchEngineConfigInterface
     */
    private $searchEngineConfig;

    /**
     * @var int
     */
    private $waitCount;

    /**
     * @var int
     */
    private $responseTimeout;

    private $connection = null;
    private $channel = null;

    private $queue;

    /**
     * @var array
     */
    private $result;

    /**
     * TaskConsumer constructor.
     * @param LoggerInterface $logger
     * @param SearchEngineConfigInterface $searchEngineConfig
     * @param ApiSearchRequestInterface $apiSearchRequest
     * @param int $waitCount
     * @param int $responseTimeout
     * @throws IWException
     */
    public function __construct(
        LoggerInterface $logger,
        SearchEngineConfigInterface $searchEngineConfig,
        ApiSearchRequestInterface $apiSearchRequest,
        int $waitCount,
        int $responseTimeout
    ) {
        $this->logger = $logger;
        $this->searchEngineConfig = $searchEngineConfig;
        $this->waitCount = $waitCount;
        $this->responseTimeout = $responseTimeout;

        $this->connection = new AMQPStreamConnection(
            $searchEngineConfig->getAmqpHost(),
            $searchEngineConfig->getAmqpPort(),
            $searchEngineConfig->getAmqpUser(),
            $searchEngineConfig->getAmqpPassword()
        );

        if ($this->connection->isConnected()) {
            $this->channel = $this->connection->channel();
            $amqpEngine = new AmqpEngine($searchEngineConfig, $this->channel);
            $this->queue = $amqpEngine->getQueueTaskResponse($apiSearchRequest->getServiceType(), $apiSearchRequest->getRequestID());
        } else {
            throw new IWException(EM::MES_AMPQ_IS_NOT_CONNECT);
        }
    }

    /**
     * TaskConsumer destructor.
     * @throws Exception
     */
    public function __destruct() {
        if (!is_null($this->channel)) {
            $this->channel->close();
        }
        if (!is_null($this->connection)) {
            $this->connection->close();
        }
    }

    /**
     * Receive a message from the Response queue (Supplier response).
     * @param AMQPMessage $message
     */
    public function consume(AMQPMessage $message) {
        $this->result[] = $message->body;
        //$this->channel->basic_ack($message->delivery_info['delivery_tag']);
    }

    /**
     * Main method. Get all supplier responses.
     * @return array Supplier responses
     * @throws ErrorException
     */
    public function go(): array {
        $this->result = [];
        $this->channel->basic_consume($this->queue, '', false, true, false, false, [$this, 'consume']);

        $currentCount = 0;
        do {
            $this->channel->wait(null, false, $this->responseTimeout);
            $currentCount++;
        } while ($currentCount != $this->waitCount);
        //while ($this->channel->is_consuming())
        return $this->result;
    }
}
