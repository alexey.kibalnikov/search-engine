<?php
/**
 * ConsumerLog.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @category Common
 * @package SearchEngine
 */

namespace iWeekender\SearchEngine;

use iWeekender\Contract\DataBase\MySQLAdapterInterface;
use iWeekender\SearchEngine\Exceptions\ExceptionMessagesInterface as EM;
use iWeekender\Contract\Configs\SearchEngineConfigInterface;
use iWeekender\Exceptions\ExceptionHandler;
use iWeekender\Exceptions\IWException;
use iWeekender\Worker\Search\Log\LogWorker;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Exception;
use ErrorException;
use Throwable;

/**
 * [queue: "log"] Logging all AMQP message from queue (use LogWorker).
 */
final class ConsumerLog
{
    private $connection = null;
    private $channel = null;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MySQLAdapterInterface
     */
    private $mySQLAdapter;

    /**
     * @var SearchEngineConfigInterface;
     */
    private $searchEngineConfig;

    /**
     * @var AmqpEngine
     */
    private $amqpEngine;

    /**
     * ConsumerLog constructor.
     * @param LoggerInterface $logger
     * @param MySQLAdapterInterface $mySQLAdapter
     * @param SearchEngineConfigInterface $searchEngineConfig
     * @throws IWException
     */
    public function __construct(
        LoggerInterface $logger,
        MySQLAdapterInterface $mySQLAdapter,
        SearchEngineConfigInterface $searchEngineConfig
    ) {
        $this->logger = $logger;
        $this->mySQLAdapter = $mySQLAdapter;
        $this->searchEngineConfig = $searchEngineConfig;


        $this->connection = new AMQPStreamConnection(
            $searchEngineConfig->getAmqpHost(),
            $searchEngineConfig->getAmqpPort(),
            $searchEngineConfig->getAmqpUser(),
            $searchEngineConfig->getAmqpPassword()
        );

        if ($this->connection->isConnected()) {
            $this->channel = $this->connection->channel();

            $this->amqpEngine = new AmqpEngine($searchEngineConfig, $this->channel);
            $this->amqpEngine->createQueueLog();
        } else {
            throw new IWException(EM::MES_AMPQ_IS_NOT_CONNECT);
        }
    }

    /**
     * ConsumerLog destructor.
     * @throws Exception
     */
    public function __destruct() {
        if (!is_null($this->channel)) {
            $this->channel->close();
        }
        if (!is_null($this->connection)) {
            $this->connection->close();
        }
    }

    /**
     * Logging AMQP message (use LogWorker).
     * @param AMQPMessage $message
     */
    public function consume(AMQPMessage $message) {
        try {
            $worker = new LogWorker($this->mySQLAdapter, $message);
            $worker->run();
            //  $this->channel->basic_ack($message->delivery_info['delivery_tag']);
        } catch (Throwable $t) {
            new ExceptionHandler($this->logger, $t);
        }
    }

    /**
     * Main method. Logging all AMQP message from queue.
     * @throws ErrorException
     */
    public function go() {
        $this->channel->basic_consume($this->amqpEngine->getQueueLog(), '', false, true, false, false, [$this, 'consume']);
        while (true) {
            $this->channel->wait();
        }
    }
}
