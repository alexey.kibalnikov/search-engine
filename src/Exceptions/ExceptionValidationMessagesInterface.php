<?php
/**
 * ExceptionValidationMessagesInterface.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @category Common
 * @package SearchEngine\Exceptions
 */

namespace iWeekender\SearchEngine\Exceptions;

/**
 * Validation Error Messages (for logged and User).
 *
 * Validation errors <b>are displayed by the user</b>, because the reason for their occurrence is the user.
 */
interface ExceptionValidationMessagesInterface
{
    const MES_SERP_ID_NOT_FOUND_IN_SEARCH_ENGINE = "SerpID not found in SearchEngine Responses";
}
