<?php
/**
 * ExceptionMessagesInterface.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @category Common
 * @package SearchEngine\Exceptions
 */

namespace iWeekender\SearchEngine\Exceptions;

/**
 * Runtime Error Messages (for logged).
 *
 * Runtime errors <b>will be hidden</b> from the user and the response API will not be given.
 */
interface ExceptionMessagesInterface
{
    const MES_AMPQ_IS_NOT_CONNECT = "AMPQ isn't connect";
    const MES_SERP_ID_NOT_FOUND_IN_SEARCH_ENGINE = "SerpID not found in SearchEngine Responses";
}
