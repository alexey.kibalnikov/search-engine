<?php
/**
 * ExceptionConfigurationMessagesInterface.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @category Common
 * @package SearchEngine\Exceptions
 */

namespace iWeekender\SearchEngine\Exceptions;

/**
 * Configuration Error Messages (for logged).
 *
 * These errors are due to <b>inconsistencies in the settings of different services</b> in the system.
 * Configuration errors <b>will be hidden</b> from the user and the response API will not be given.
 */
interface ExceptionConfigurationMessagesInterface
{
    const MES_AMPQ_CONF_GET_TASK_REQUEST_ARG  = "AMPQConf ARG_TASK_REQUEST_<%s> not implemented";
    const MES_AMPQ_CONF_GET_TASK_RESPONSE_ARG = "AMPQConf ARG_TASK_RESPONSE_<%s> not implemented";
}
