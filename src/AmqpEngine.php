<?php
/**
 * AmqpEngine.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @category Common
 * @package SearchEngine
 */

namespace iWeekender\SearchEngine;

use iWeekender\Contract\ImportExport\DataFormatEnumInterface;
use iWeekender\SearchEngine\Exceptions\ExceptionConfigurationMessagesInterface as ECM;
use iWeekender\Contract\Request\ServiceTypeEnumInterface as STE;
use iWeekender\Contract\Configs\SearchEngineConfigInterface;
use iWeekender\Utils\Response\ResponseHelper;
use iWeekender\Exceptions\IWExceptionConfiguration;
use function iWeekender\Contract\jsonToArray;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Channel\AMQPChannel;
use GuzzleHttp\Client as GuzzleHttpClient;

/**
 * AMQP Wrapper and Adapter.
 */
final class AmqpEngine
{
    /**
     * @var SearchEngineConfigInterface
     */
    private $searchEngineConfig;

    /**
     * @var AMQPChannel
     */
    private $channel;

    private $apiQueue;

    /**
     * @var array
     */
    private $apiAuth;

    public function __construct(SearchEngineConfigInterface $searchEngineConfig, AMQPChannel $channel) {
        $this->searchEngineConfig = $searchEngineConfig;
        $this->channel = $channel;

        $host = $this->searchEngineConfig->getAmqpHost();
        $port = $this->searchEngineConfig->getAmqpApiPort();
        $this->apiQueue  = new GuzzleHttpClient(['base_uri' => $host . ':' . $port]);

        $user = $this->searchEngineConfig->getAmqpUser();
        $password = $this->searchEngineConfig->getAmqpPassword();
        $this->apiAuth = ['auth' => [$user, $password]];
    }

    /**
     * @param string $queueName
     * @return bool
     */
    public function isQueueExist(string $queueName) {
        $rawSearch = $this->apiQueue->get('/api/queues', $this->apiAuth);

        $queues = jsonToArray($rawSearch->getBody());
        foreach ($queues as $queue) {
            if ($queue['name'] === $queueName) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $queueName
     * @param int $maxLength
     * @param int $messageTTL
     * @return bool
     */
    public function isSameConfigurationQueue(string $queueName, int $maxLength, int $messageTTL) {
        $rawSearch = $this->apiQueue->get('/api/queues', $this->apiAuth);

        $queues = jsonToArray($rawSearch->getBody());
        foreach ($queues as $queue) {
            if ($queue['name'] === $queueName) {
                return $queue['arguments']['x-max-length'] == $maxLength && $queue['arguments']['x-message-ttl'] == $messageTTL;
            }
        }
        return false;
    }

    /**
     * @param string $serviceType
     * @return string
     */
    public function getQueueTaskRequest(string $serviceType): string {
        return sprintf($this->searchEngineConfig->getQueueTaskRequest(), $serviceType);
    }

    /**
     * @param string $serviceType
     * @param string $requestID
     * @return string
     */
    public function getQueueTaskResponse(string $serviceType, string $requestID): string {
        return sprintf($this->searchEngineConfig->getQueueTaskResponse(), $serviceType, $requestID);
    }

    /**
     * @return string
     */
    public function getQueueLog(): string {
        return $this->searchEngineConfig->getQueueLog();
    }

    /**
     * @param $serviceType
     * @return string
     */
    public function getExchangeTask($serviceType): string {
        return sprintf($this->searchEngineConfig->getExchangeTask(), $serviceType);
    }

    /*  INT_LONG    => 'I',
     *  DECIMAL     => 'D',
     *  TIMESTAMP   => 'T',
     *  STRING_LONG => 'S',
     *  TABLE       => 'F'
     */

    /**
     * @param string $serviceType
     * @return array
     * @throws IWExceptionConfiguration
     */
    public function getTaskRequestArg(string $serviceType) {
        if ($serviceType === STE::FLIGHT) {
            return [
                'x-message-ttl' => ['I', $this->searchEngineConfig->getAmqpTaskRequestMesTTL() * 60 * 1000], /*milliseconds*/
                'x-max-length'  => ['I', $this->searchEngineConfig->getAmqpTaskRequestMaxLenght()]
            ];
        } else {
            throw new IWExceptionConfiguration(sprintf(ECM::MES_AMPQ_CONF_GET_TASK_REQUEST_ARG, $serviceType));
        }
    }

    /**
     * @param string $serviceType
     * @return array
     * @throws IWExceptionConfiguration
     */
    public function getTaskResponseArg(string $serviceType) {
        if ($serviceType === STE::FLIGHT) {
            return [
                'x-expires'     => ['I', $this->searchEngineConfig->getAmqpTaskResponseExpires() * 60 * 1000], /*milliseconds*/
                'x-message-ttl' => ['I', $this->searchEngineConfig->getAmqpTaskResponseMessageTTL() * 60 * 1000], /*milliseconds*/
                'x-max-length'  => ['I', $this->searchEngineConfig->getAmqpTaskResponseMaxLenght()]
            ];
        } else {
            throw new IWExceptionConfiguration(sprintf(ECM::MES_AMPQ_CONF_GET_TASK_RESPONSE_ARG, $serviceType));
        }
    }

    /**
     * @return array
     */
    public function getLogArg() {
        return [
            'x-message-ttl' => ['I', $this->searchEngineConfig->getAmqpTaskLogMessageTTL() * 60 * 1000], /*milliseconds*/
            'x-max-length'  => ['I', $this->searchEngineConfig->getAmqpTaskLogMaxLenght()]
        ];
    }

    const AMQP_MSG_DELIVERY_MODE_NON_PERSISTEN = 1;
    const AMQP_MSG_DELIVERY_MODE_PERSISTEN = 2;

    /**
     * @param string $serviceType
     * @param string $requestID
     * @param string $correlationID
     * @return array
     */
    private function getAmqpMessageProperties(string $serviceType, string $requestID, string $correlationID) {
        return [
            'content_type'   => DataFormatEnumInterface::JSON,
            'delivery_mode'  => self::AMQP_MSG_DELIVERY_MODE_NON_PERSISTEN,
            'correlation_id' => $correlationID,
            'reply_to'       => $this->getQueueTaskResponse($serviceType, $requestID),
            'message_id'     => ResponseHelper::getUniqueStr(),
            'timestamp'      => time(),
            'type'           => $serviceType,
            'app_id'         => 'iwServiceProvider'
        ];
    }

    /**
     * @param string $data
     * @param string $serviceType
     * @param string $requestID
     * @param string $correlationID
     * @return AMQPMessage
     */
    public function createAmqpMessage(string $data, string $serviceType, string $requestID, string $correlationID = '-1'): AMQPMessage {
        return new AMQPMessage($data, $this->getAMQPMessageProperties($serviceType, $requestID, $correlationID));
    }

    /**
     * @param string $serviceType
     * @throws IWExceptionConfiguration
     */
    public function createQueueTaskRequest(string $serviceType): void {
        $queueName = $this->getQueueTaskRequest($serviceType);
        $isExistQueue = $this->isQueueExist($queueName);

        $isSameConfigurationQueue = $this->isSameConfigurationQueue(
            $queueName,
            $this->searchEngineConfig->getAmqpTaskRequestMaxLenght(),
            $this->searchEngineConfig->getAmqpTaskRequestMesTTL() * 60 * 1000
        );

        $shouldDeleteQueue = $isExistQueue && !$isSameConfigurationQueue;

        $shouldCreateQueue = !$isExistQueue || $shouldDeleteQueue;

        if ($shouldDeleteQueue) {
            $this->channel->queue_delete($queueName);
        }

        if ($shouldCreateQueue || $shouldDeleteQueue) {
            /*durable = true*/
            $this->channel->queue_declare($queueName, false, true, false, false, false, $this->getTaskRequestArg($serviceType));
        }
    }

    public function createQueueLog(): void {
        $queueName = $this->getQueueLog();
        $isExistQueue = $this->isQueueExist($queueName);

        $isSameConfigurationQueue = $this->isSameConfigurationQueue(
            $queueName,
            $this->searchEngineConfig->getAmqpTaskLogMaxLenght(),
            $this->searchEngineConfig->getAmqpTaskLogMessageTTL() * 60 * 1000
        );

        $shouldDeleteQueue = $isExistQueue && !$isSameConfigurationQueue;

        $shouldCreateQueue = !$isExistQueue || $shouldDeleteQueue;

        if ($shouldDeleteQueue) {
            $this->channel->queue_delete($queueName);
        }

        if ($shouldCreateQueue || $shouldDeleteQueue) {
            /*durable = true*/
            $this->channel->queue_declare($queueName, false, true, false, false, false, $this->getLogArg());
        }
    }
}
