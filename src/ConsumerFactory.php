<?php
/**
 * ConsumerFactory.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @category Common
 * @package SearchEngine
 */

namespace iWeekender\SearchEngine;

use iWeekender\SearchEngine\Exceptions\ExceptionMessagesInterface as EM;
use iWeekender\Contract\Configs\SearchEngineConfigInterface;
use iWeekender\Contract\Configs\SuppliersApiConfigInterface;
use iWeekender\Utils\Response\Response;
use iWeekender\Worker\Search\WorkerFactory;
use iWeekender\Exceptions\ExceptionHandler;
use iWeekender\Exceptions\IWException;
use iWeekender\Exceptions\IWExceptionConfiguration;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Exception;
use ErrorException;
use Throwable;

/**
 * [queue: "task.flight.response.id"] Request FlightRate Collection from suppliers (use Worker).
 */
final class ConsumerFactory
{
    private $connection = null;
    private $channel = null;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SearchEngineConfigInterface;
     */
    private $searchEngineConfig;

    /**
     * @var SuppliersApiConfigInterface
     */
    private $suppliersApiConfig;

    /**
     * @var string;
     */
    private $serviceType;

    /**
     * @var AmqpEngine
     */
    private $amqpEngine;

    /**
     * ConsumerFactory constructor.
     * @param LoggerInterface $logger
     * @param SearchEngineConfigInterface $searchEngineConfig
     * @param SuppliersApiConfigInterface $suppliersApiConfig
     * @param string $serviceType
     * @throws IWException
     * @throws IWExceptionConfiguration
     */
    public function __construct(
        LoggerInterface $logger,
        SearchEngineConfigInterface $searchEngineConfig,
        SuppliersApiConfigInterface $suppliersApiConfig,
        string $serviceType
    ) {
        $this->logger = $logger;
        $this->searchEngineConfig = $searchEngineConfig;
        $this->suppliersApiConfig = $suppliersApiConfig;
        $this->serviceType = $serviceType;

        $this->connection = new AMQPStreamConnection(
            $searchEngineConfig->getAmqpHost(),
            $searchEngineConfig->getAmqpPort(),
            $searchEngineConfig->getAmqpUser(),
            $searchEngineConfig->getAmqpPassword()
        );

        if ($this->connection->isConnected()) {
            $this->channel = $this->connection->channel();
            /*global = false - shared across all consumers on the channel
                       true  - shared across all consumers on the connection */
            $this->channel->basic_qos(null, $searchEngineConfig->getAmqpPrefetchCount(), false);
            $this->amqpEngine = new AmqpEngine($searchEngineConfig, $this->channel);
            $this->amqpEngine->createQueueTaskRequest($this->serviceType);
        } else {
            throw new IWException(EM::MES_AMPQ_IS_NOT_CONNECT);
        }
    }

    /**
     * ConsumerFactory destructor.
     * @throws Exception
     */
    public function __destruct() {
        if (!is_null($this->channel)) {
            $this->channel->close();
        }
        if (!is_null($this->connection)) {
            $this->connection->close();
        }
    }

    /**
     * Request FlightRate Collection from supplier (use Worker).
     * @param AMQPMessage $message
     */
    public function consume(AMQPMessage $message) {
        try {
            $workerFactory = new WorkerFactory($this->suppliersApiConfig, $message);
            $worker = $workerFactory->getWorker();
            try {
                $data = $worker->run();
            } catch (Throwable $t) {
                $data = Response::EXCEPTION;
                new ExceptionHandler($this->logger, $t);
            } finally {
                /* The Supplier's API response must be in the response queue even if the supplier has responded with an Exception */
                $result = $this->amqpEngine->createAMQPMessage($data, $this->serviceType, $worker->getSearchRequest()->getRequestId(), $worker->getAmqpMessageID());

                foreach ([$worker->getAmqpReplyTo(), $this->amqpEngine->getQueueLog()] as $routingKey) {
                    // Do not include blank data in the log
                    if ($routingKey === $this->amqpEngine->getQueueLog() && (empty($data) || $data === Response::EXCEPTION)) {
                        continue;
                    }
                    $this->channel->basic_publish($result, '', $routingKey);
                }
                //$this->channel->basic_ack($message->delivery_info['delivery_tag']);
            }
        } catch (Throwable $t) {
            new ExceptionHandler($this->logger, $t);
        }
    }

    /**
     * Main method. Request FlightRate Collection from suppliers.
     * @throws ErrorException
     */
    public function go() {
        $this->channel->basic_consume($this->amqpEngine->getQueueTaskRequest($this->serviceType), '', false, true, false, false, [$this, 'consume']);
        while (true) {
            $this->channel->wait();
        }
    }
}
