<?php
/**
 * SQLHelper.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @category Common
 * @package SearchEngine
 */

namespace iWeekender\SearchEngine;

use iWeekender\Contract\DataBase\MySQLAdapterInterface;
use iWeekender\SearchEngine\Exceptions\ExceptionValidationMessagesInterface as EVM;
use iWeekender\DataModels\Flight\Rate;
use iWeekender\Exceptions\IWException;
use iWeekender\Exceptions\IWExceptionValidation;
use Doctrine\DBAL\DBALException;

/**
 * Contains all methods of working with databases. Working with the database directly ideologically incorrect.
 */
class SQLHelper
{
    /**
     * @var MySQLAdapterInterface
     */
    private $mySQLAdapter;

    /**
     * SQLHelper constructor.
     * @param MySQLAdapterInterface $mySQLAdapter
     */
    public function __construct(MySQLAdapterInterface $mySQLAdapter) {
        $this->mySQLAdapter = $mySQLAdapter;
    }

    /**
     * Call SQL Procedure "SELECT_FLIGHT_SUPPLIER_RATES". Find suppliers' FlightRate in the database.
     * @param string $serpID
     * @return string
     * @throws DBALException
     * @throws IWExceptionValidation
     */
    private function getRateDataBySerpId(string $serpID): string {
        $query = "CALL SELECT_FLIGHT_SUPPLIER_RATES(:serp_id)";
        $row = $this->mySQLAdapter->executeQueryFetch($query, ['serp_id' => $serpID]);
        if (isset($row['json'])) {
            $result = $row['json'];
        } else {
            throw new IWExceptionValidation(EVM::MES_SERP_ID_NOT_FOUND_IN_SEARCH_ENGINE);
        }
        return $result;
    }

    /**
     * Return found FlightRate ("as is" Supplier ID).
     * @param string $serpID
     * @return Rate
     * @throws DBALException
     * @throws IWException
     * @throws IWExceptionValidation
     */
    public function getRateBySerpId(string $serpID): Rate {
        $data = $this->getRateDataBySerpId($serpID);
        $flightRate = new Rate();
        $flightRate->importData($data);
        return $flightRate;
    }
}
